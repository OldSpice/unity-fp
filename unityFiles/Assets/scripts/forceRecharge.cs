﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class forceRecharge : MonoBehaviour {

	public float forceMultipler;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            forceGun.SetOverChargeShot(forceMultipler);
        }
    }
}
