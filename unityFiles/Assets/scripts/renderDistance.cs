﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class renderDistance : MonoBehaviour {

    Transform player;
    ParticleSystem myEmit;

    private void Start()
    {
        player = GameObject.Find("player").transform;
        myEmit = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        float distance = (player.position - transform.position).magnitude;

        if (distance > 40)
        {
            myEmit.Stop();
        } else
        {
            myEmit.Play();
        }
	}
}
