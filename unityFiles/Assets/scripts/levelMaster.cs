﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelMaster : MonoBehaviour {

    public Vector3 playerSpawnPosition;
    public Quaternion playerSpawnRotation;

    Transform playerTransform;

    private void OnEnable()
    {
        playerTransform = GameObject.Find("player").transform;
        playerTransform.GetComponent<playerController>().SetCurrentMaster(gameObject);
    }

    public Vector3 getPlayerSpawnPosition()
    {
        return playerSpawnPosition;
    }
    public Quaternion getPlayerSpawnRotation()
    {
        return playerSpawnRotation;
    }

}
