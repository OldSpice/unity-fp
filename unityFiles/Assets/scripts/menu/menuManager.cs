﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuManager : MonoBehaviour {


    Text myText;
    public Transform myMenuCanvas;
    Vector3 startPosition;

    GameObject menuMasterObject;

    levelHolder manager;
    Transform playerTransform;
    Rigidbody playerRigidbody;

    private void Start()
    {
        myText = transform.GetComponent<Text>();

        menuMasterObject = GameObject.Find("menu");

        playerTransform = GameObject.Find("player").transform;
        playerRigidbody = playerTransform.GetComponent<Rigidbody>();

        manager = GameObject.Find("manager").GetComponent<levelHolder>();

    }

    bool setDefaultPosition = true;
    private void Awake()
    {
        if (setDefaultPosition)
        {
            startPosition = transform.localPosition;
            setDefaultPosition = false;
        }
    }
    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (setDefaultPosition)
        {
            startPosition = transform.localPosition;
            setDefaultPosition = false;
        }
        CloseLayer();
        CloseTab();
    }

    public void IsHighlighted()
    {
        myText.color = new Color(204 / 255F, 51 / 255F, 51 / 255F);
    }
    public void NotHighlighted()
    {
        myText.color = new Color(191 / 255F, 191 / 255F, 191 / 255F);
    }

    public void OpenTab()
    {
        //Close all tabs
        for (int i = 0; i < myMenuCanvas.childCount; i++)
        {
            myMenuCanvas.GetChild(i).GetComponent<Text>().GetComponent<menuManager>().CloseTab();
            if (myMenuCanvas.GetChild(i).childCount > 0)
            {
                for (int j = 0; j < myMenuCanvas.GetChild(i).childCount; j++)
                {
                    myMenuCanvas.GetChild(i).GetChild(j).GetComponent<Text>().GetComponent<menuManager>().CloseLayer();
                }
            }
        }
        if (transform.childCount > 0)
        {
            //Move menu items below selection down
            for (int i = 0 + transform.GetSiblingIndex() + 1; i < myMenuCanvas.childCount; i++)
            {
                myMenuCanvas.GetChild(i).localPosition -= new Vector3(0, 41.3F * transform.childCount, 0);
            }
            //Show menu tab sub options
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }
    public void OpenLayer()
    {
        if (transform.childCount > 0)
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
                transform.parent.GetChild(i).GetComponent<menuManager>().CloseLayer();
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    public void CloseTab()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        transform.localPosition = startPosition;
    }
    public void CloseLayer()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void ResumeGame()
    {
        //Close all tabs //WORKING?
        for (int i = 0; i < myMenuCanvas.childCount; i++)
        {
            myMenuCanvas.GetChild(i).GetComponent<Text>().GetComponent<menuManager>().CloseTab();
        }
        menuMaster.SetPauseMenuOpen(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        myMenuCanvas.gameObject.SetActive(false);
    }

    public void AdjustXSensitivity(float newValue)
    {
        mouseAndKeyboardSettings.mouseSensitivityX = newValue;
    }
    public void AdjustYSensitivity(float newValue)
    {
        mouseAndKeyboardSettings.mouseSensitivityY = newValue;
    }

    bool listenForKey, bindKey = false;
    string keyInput = "";
    string keyToBind;

    private void Update()
    {
        if (listenForKey)
        {
            keyInput = Input.inputString;
        }
        if (keyInput != "")
        {
            bindKey = true;
        }
        if (bindKey)
        {
            KeyCode thisKeyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), keyInput, true);
            switch (keyToBind)
            {
                case "Forward":
                    mouseAndKeyboardSettings.forwardKey = thisKeyCode;
                    break;
                case "Left":
                    mouseAndKeyboardSettings.strafeLeftKey = thisKeyCode;
                    break;
                case "Right":
                    mouseAndKeyboardSettings.strafeRightKey = thisKeyCode;
                    break;
                case "Backwards":
                    mouseAndKeyboardSettings.backwardsKey = thisKeyCode;
                    break;
                case "Interact":
                    mouseAndKeyboardSettings.interactKey = thisKeyCode;
                    break;
                default:
                    break;    
            }
            keyInput = "";
            listenForKey = false;
            bindKey = false;
        }
    }

    public void BindForwardKey()
    {
        keyToBind = "Forward";
        listenForKey = true;
    }
    public void BindLeftKey()
    {
        keyToBind = "Left";
        listenForKey = true;
    }
    public void BindRightKey()
    {
        keyToBind = "Right";
        listenForKey = true;
    }
    public void BindBackwardKey()
    {
        keyToBind = "Backwards";
        listenForKey = true;
    }
    public void BindInteractdKey()
    {
        keyToBind = "Interact";
        listenForKey = true;
    }
    public void LoadLaunchMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void AdjustVolume(float newVolume)
    {
        musicController.audioLevel = newVolume;

    }

    public void ShowLevelPreview()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
    public void HideLevelPreview()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void LoadLevel001()
    {
        manager.UnLoadAllLevels();
        manager.level001.SetActive(true);
        manager.level001Master.SetActive(true);
        manager.level001Transition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }

    public void LoadLevel002()
    {
        manager.UnLoadAllLevels();
        manager.level001Transition.SetActive(true);
        manager.level002.SetActive(true);
        manager.level002Master.SetActive(true);
        manager.level002Transition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }
    public void LoadLevel003()
    {
        manager.UnLoadAllLevels();
        manager.level002Transition.SetActive(true);
        manager.level003.SetActive(true);
        manager.level003Master.SetActive(true);
        manager.level003Transition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }
    public void LoadLevel004()
    {
        manager.UnLoadAllLevels();
        manager.level003Transition.SetActive(true);
        manager.level004.SetActive(true);
        manager.level004Master.SetActive(true);
        manager.level004Transition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }
    public void LoadLevel005()
    {
        manager.UnLoadAllLevels();
        manager.level004Transition.SetActive(true);
        manager.level005.SetActive(true);
        manager.level005Master.SetActive(true);
        manager.level005Transition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }

    public void LoadLevel00E()
    {
        manager.UnLoadAllLevels();
        manager.level003Transition.SetActive(true);
        manager.level00E.SetActive(true);
        manager.level00EMaster.SetActive(true);
        manager.level00ETransition.SetActive(true);
        playerTransform.gameObject.SetActive(true);
        playerTransform.GetComponent<playerController>().ResetPlayer();
        playerRigidbody.GetComponent<Rigidbody>().useGravity = true;
        menuMaster.SetLaunchMenuOpen(false);
        ResumeGame();
    }

}
