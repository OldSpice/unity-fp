﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuDisplayManager : MonoBehaviour {

    Text display;

    public string text;

    public bool forwardKey;
    public bool leftKey;
    public bool rightKey;
    public bool backwardKey;
    public bool interactKey;

    public bool mouseX;
    public bool mouseY;

    public bool musicLevel;

    private void Start()
    {
        display = transform.GetComponent<Text>();
    }

    void Update () {
        if(forwardKey){
            display.text = text + mouseAndKeyboardSettings.forwardKey;
        }
        if (leftKey)
        {
            display.text = text + mouseAndKeyboardSettings.strafeLeftKey;
        }
        if (rightKey)
        {
            display.text = text + mouseAndKeyboardSettings.strafeRightKey;
        }
        if (backwardKey)
        {
            display.text = text + mouseAndKeyboardSettings.backwardsKey;
        }
        if (interactKey)
        {
            display.text = text + mouseAndKeyboardSettings.interactKey;
        }
        if (mouseX)
        {
            display.text = text + Mathf.Round(mouseAndKeyboardSettings.mouseSensitivityX * 100f) / 100f;
        }
        if (mouseY)
        {
            display.text = text + Mathf.Round(mouseAndKeyboardSettings.mouseSensitivityY * 100f) / 100f;
        }
        if (musicLevel)
        {
            display.text = text + Mathf.Round(musicController.audioLevel * 100f) / 100f;
        }
    }
    
}
