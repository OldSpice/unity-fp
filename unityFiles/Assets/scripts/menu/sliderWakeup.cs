﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderWakeup : MonoBehaviour {
    public Slider mySlider;

    public bool mouseX;
    public bool mouseY;

    public bool music;

    private void OnEnable()
    {
        mySlider.gameObject.SetActive(true);
        if (mouseX) {
            mySlider.value = mouseAndKeyboardSettings.mouseSensitivityX;
        }
        if (mouseY)
        {
            mySlider.value = mouseAndKeyboardSettings.mouseSensitivityY;
        }
        if (music)
        {
            mySlider.value = musicController.audioLevel;
        }
    }
    private void OnDisable()
    {
        mySlider.gameObject.SetActive(false);
    }
}
