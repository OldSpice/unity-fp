﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuMaster : MonoBehaviour {

    static bool menuOpen = true;
    static bool launchMenuOpen = true;
    static bool pauseMenuOpen = false;
    public GameObject pauseMenuCanvas;
    public GameObject launchMenuCanvas;

    GameObject forceGunCamera;
    Transform player;


    private void Start()
    {
        player = GameObject.Find("player").transform;
        forceGunCamera = player.Find("playerCameraE").gameObject;

    }


    private void Update () {
        if (launchMenuOpen || pauseMenuOpen)
        {
            menuOpen = true;
        } else
        {
            menuOpen = false;
        }
        if (launchMenuOpen)
        {
            forceGunCamera.SetActive(false);
            player.position = new Vector3(0, 200, 0);
            player.rotation = new Quaternion(0, 0, 0, 0);
            player.GetComponent<Rigidbody>().useGravity = false;

        }
        else
        {
            forceGunCamera.SetActive(true);
            player.GetComponent<Rigidbody>().useGravity = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseMenuOpen && !launchMenuOpen){
            pauseMenuCanvas.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            pauseMenuOpen = true;
        }
        if (pauseMenuOpen)
        {
            Time.timeScale = 0;
        } else
        {
            Time.timeScale = 1;
        }
	}

    public static bool GetMenuOpen()
    {
        return menuOpen;
    }
    public static bool GetLaunchMenuOpen()
    {
        return launchMenuOpen;
    }
    public static void SetLaunchMenuOpen(bool boolean)
    {
        launchMenuOpen = boolean;
    }

    public static bool GetPauseMenuOpen()
    {
        return pauseMenuOpen;
    }
    public static void SetPauseMenuOpen(bool boolean)
    {
        pauseMenuOpen = boolean;
    }
}
