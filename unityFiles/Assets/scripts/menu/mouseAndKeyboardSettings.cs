﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseAndKeyboardSettings : MonoBehaviour {

    public static KeyCode forwardKey = KeyCode.W;
    public static KeyCode strafeRightKey = KeyCode.D;
    public static KeyCode strafeLeftKey = KeyCode.A;
    public static KeyCode backwardsKey = KeyCode.S;

    public static KeyCode interactKey = KeyCode.E;

    public static KeyCode pauseKey = KeyCode.Escape;

    public static float mouseSensitivityX = 3;
    public static float mouseSensitivityY = 3;

}
