﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{

    Rigidbody myRigidbody;
    CapsuleCollider groundedTrigger;

    public float movementSpeed = 30;

    public float maxSpeed;

    float distanceToGround = 0.47f;

    [HideInInspector] public bool isDead;

    GameObject levelMaster;

    public AudioClip[] soundClips;
    AudioSource soundSource;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();

        soundSource = GetComponent<AudioSource>();
        soundSource.loop = false;
    }
    private void Update()
    {
        IsGrounded();
        if (IsGrounded())
        {
            movementSpeed = 30;
        } else
        {
            movementSpeed = 2;
        }
    }
    void FixedUpdate()
    {
        if (!menuMaster.GetMenuOpen())
        {
            if (Input.GetKey(mouseAndKeyboardSettings.forwardKey))
            {
                myRigidbody.AddForce(transform.forward * movementSpeed);
            }
            if (Input.GetKey(mouseAndKeyboardSettings.strafeLeftKey))
            {
                myRigidbody.AddForce(-transform.right * movementSpeed);
            }
            if (Input.GetKey(mouseAndKeyboardSettings.backwardsKey))
            {
                myRigidbody.AddForce(-transform.forward * movementSpeed);
            }
            if (Input.GetKey(mouseAndKeyboardSettings.strafeRightKey))
            {
                myRigidbody.AddForce(transform.right * movementSpeed);
            }
            if (!Input.GetKey(mouseAndKeyboardSettings.forwardKey) && !Input.GetKey(mouseAndKeyboardSettings.strafeLeftKey) && !Input.GetKey(mouseAndKeyboardSettings.strafeRightKey) && !Input.GetKey(mouseAndKeyboardSettings.backwardsKey) && IsGrounded())
            {
                myRigidbody.velocity *= 0.9f;
            }
            if (myRigidbody.velocity.magnitude > maxSpeed && IsGrounded())
            {
                float keepYVelocity = myRigidbody.velocity.y;
                myRigidbody.velocity = myRigidbody.velocity.normalized * maxSpeed;
                myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, keepYVelocity, myRigidbody.velocity.z);
            }
        }

        if (isDead)
        {
            soundSource.clip = soundClips[0];
            soundSource.Play();
            ResetPlayer();
            forceGun.SetOverChargeShot(1);
            isDead = false;
        }
    }

    bool slipping = false;
    private void OnCollisionEnter(Collision other)
    {
        if(other.collider.tag == "slippery")
        {
            movementSpeed = 0.5f;
            maxSpeed = 10;
            slipping = true;
        } else 
        {
            movementSpeed = 30;
            maxSpeed = 5;
            slipping = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "killZone")
        {
            isDead = true;
        }
    }
    private bool IsGrounded()
    {
        if (!slipping)
        {
            if(Physics.Raycast(transform.position, -Vector3.up, distanceToGround + 0.1f) ||
                Physics.Raycast(transform.position + -transform.forward * 0.2f, -Vector3.up, distanceToGround + 0.2f) ||
                Physics.Raycast(transform.position + transform.forward * 0.2f, -Vector3.up, distanceToGround + 0.2f) ||
                Physics.Raycast(transform.position + -transform.right * 0.2f, -Vector3.up, distanceToGround + 0.2f) ||
                Physics.Raycast(transform.position + transform.right * 0.2f, -Vector3.up, distanceToGround + 0.2f))
            return true;
        } else
        {
            return false;
        }

        return false;
    }

    public void SetCurrentMaster(GameObject levelMaster)
    {
        this.levelMaster = levelMaster;
    }

    public void ResetPlayer()
    {
        transform.position = levelMaster.GetComponent<levelMaster>().getPlayerSpawnPosition();
        transform.rotation = levelMaster.GetComponent<levelMaster>().getPlayerSpawnRotation();
    }
}
