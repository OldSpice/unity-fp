﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class itemInteractor : MonoBehaviour {
    Transform playerTransform;
    Transform playerCameraTransform;
    Transform playerForceGun;
    
    bool holdingObject = false;

    Transform grabbedObject;
    RaycastHit objectGrabRay;

    float throwForce;
    float throwBuffer;

    private void Start()
    {
        playerTransform = GameObject.Find("player").transform;
        playerCameraTransform = GameObject.Find("player/playerCamera").transform;

        playerForceGun = GameObject.Find("player/playerCamera/forceGun").transform;
        throwForce = playerForceGun.GetComponent<forceGun>().GetShotForce();
        throwBuffer = playerForceGun.GetComponent<forceGun>().GetShotBuffer();

    }

    private void Update()
    {
        if (!holdingObject)
        {
            if (Input.GetKeyDown(mouseAndKeyboardSettings.interactKey))
            {
                GrabObject(true);
            }
        }
        else
        {
            HoldObject(grabbedObject);
        }        
    }

    bool GrabObject(bool toHold)
    {
        if (Physics.Raycast(playerCameraTransform.position, playerCameraTransform.forward * 2, out objectGrabRay, 3f)) {
            grabbedObject = objectGrabRay.transform;
            try {
                if (grabbedObject.GetComponent<objectCarryInformation>().canTake) {
                    holdingObject = toHold;
                    return true;
                }
            } catch (NullReferenceException) {
                return false;
            }
            
        }
        return false;

    }

    public float objRotationLerpSpeed = 0;
    public float objPositionLerpSpeed = 0;

    void HoldObject(Transform grabbedObject)
    {
        try
        {
            grabbedObject.GetComponent<objectCarryInformation>().SetIsHeld(true);
            if (grabbedObject.GetComponent<objectCarryInformation>().canHold)
            {
                grabbedObject.transform.position = Vector3.Lerp(grabbedObject.position, playerCameraTransform.position + playerCameraTransform.forward * 2, Time.deltaTime * objPositionLerpSpeed);
                grabbedObject.GetComponent<Rigidbody>().isKinematic = true;
                Physics.IgnoreCollision(playerTransform.GetComponent<CapsuleCollider>(), grabbedObject.GetComponent<BoxCollider>(), true);
                Physics.IgnoreCollision(playerTransform.GetComponent<CapsuleCollider>(), grabbedObject.GetChild(7).GetComponent<BoxCollider>(), true);
                grabbedObject.transform.rotation = Quaternion.Lerp(grabbedObject.rotation, playerCameraTransform.rotation, objRotationLerpSpeed);
            }
        }
        catch (NullReferenceException)
        {
            holdingObject = false;
        }
        if (Input.GetMouseButtonDown(0) && forceGun.canShoot)
        {
            Vector3 keepVelocity = grabbedObject.GetComponent<Rigidbody>().velocity;
            grabbedObject.GetComponent<Rigidbody>().isKinematic = false;
            grabbedObject.GetComponent<Rigidbody>().AddForce((throwForce / throwBuffer) * playerCameraTransform.forward, ForceMode.Impulse);
            grabbedObject.GetComponent<objectCarryInformation>().SetIsHeld(false);
            Physics.IgnoreCollision(playerTransform.GetComponent<CapsuleCollider>(), grabbedObject.GetChild(7).GetComponent<BoxCollider>(), false);
            holdingObject = false;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            Vector3 keepVelocity = grabbedObject.GetComponent<Rigidbody>().velocity;
            grabbedObject.GetComponent<Rigidbody>().isKinematic = false;
            grabbedObject.GetComponent<objectCarryInformation>().SetIsHeld(false);
            Physics.IgnoreCollision(playerTransform.GetComponent<CapsuleCollider>(), grabbedObject.GetChild(7).GetComponent<BoxCollider>(), false);
            holdingObject = false;
        }
    }
}
