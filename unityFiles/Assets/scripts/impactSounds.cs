﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impactSounds : MonoBehaviour {

    public AudioClip[] impactClips;
    private AudioSource audioSource;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        audioSource.clip = impactClips[Random.Range(0, impactClips.Length)];
        audioSource.Play();
    }
}
