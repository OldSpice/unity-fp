﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorLockManager : MonoBehaviour {

    public GameObject door;
    public GameObject[] lasers;
    public bool activeIsOpen;

    public bool oneTimeActivate = false;
    public bool isDoor = true;

    Renderer buttonRenderer;
    Material active;
    Material inActive;

    private void Start()
    {
        buttonRenderer = transform.GetComponent<Renderer>();
        active = Resources.Load("gadjetsMaterial/plateActive") as Material;
        inActive = Resources.Load("gadjetsMaterial/plateInActive") as Material;
    }

    private void OnCollisionStay(Collision other)
    {
        if (isDoor)
        {
            if (other.gameObject.tag == "interactablePhysicsObject")
            {
                buttonRenderer.material = active;
                if (activeIsOpen)
                {
                    door.GetComponent<doorScript>().SetDoorLocked(false);
                }
                else
                {
                    door.GetComponent<doorScript>().SetDoorLocked(true);
                }
            }
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (isDoor) {
            if (other.gameObject.tag == "interactablePhysicsObject")
            {
                buttonRenderer.material = inActive;
                if (activeIsOpen)
                {
                    door.GetComponent<doorScript>().SetDoorLocked(true);
                } else
                {
                    door.GetComponent<doorScript>().SetDoorLocked(false);
                }
            }
        }

        if (!isDoor && !oneTimeActivate)
        {
            if (other.gameObject.tag == "interactablePhysicsObject")
            {
                buttonRenderer.material = inActive;
                foreach (GameObject myobject in lasers)
                {
                    myobject.SetActive(true);
                }
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!isDoor)
        {
            if (other.gameObject.tag == "interactablePhysicsObject")
            {
                buttonRenderer.material = active;
                foreach (GameObject myobject in lasers)
                {
                    myobject.SetActive(false);
                }
            }
        }
    }
}
