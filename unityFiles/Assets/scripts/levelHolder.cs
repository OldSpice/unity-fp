﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelHolder : MonoBehaviour {

    public GameObject level001;
    public GameObject level001Master;
    public GameObject level001Transition;

    public GameObject level002;
    public GameObject level002Master;
    public GameObject level002Transition;

    public GameObject level003;
    public GameObject level003Master;
    public GameObject level003Transition;

    public GameObject level004;
    public GameObject level004Master;
    public GameObject level004Transition;

    public GameObject level00E;
    public GameObject level00EMaster;
    public GameObject level00ETransition;

    public GameObject level005;
    public GameObject level005Master;
    public GameObject level005Transition;

    public void UnLoadAllLevels()
    {
        level001.SetActive(false);
        level001Master.SetActive(false);
        level001Transition.SetActive(false);
        level002.SetActive(false);
        level002Master.SetActive(false);
        level002Transition.SetActive(false);
        level003.SetActive(false);
        level003Master.SetActive(false);
        level003Transition.SetActive(false);
        level004.SetActive(false);
        level004Master.SetActive(false);
        level004Transition.SetActive(false);
        level005.SetActive(false);
        level005Master.SetActive(false);
        level005Transition.SetActive(false);

        level00E.SetActive(false);
        level00EMaster.SetActive(false);
        level00ETransition.SetActive(false);
    }

}
