﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelSwitcher : MonoBehaviour {

    public bool isChallengeRun;

    public GameObject currentLevel;
    public GameObject currentLevelMaster;
    public GameObject currentTransitionDoor;
    public GameObject pastLevelTransition;

    public GameObject nextLevel;
    public GameObject nextLevelMaster;
    public GameObject nextLevelTransition;

    //Transform playerTransform;

    bool transition = false;
    bool lockDoor = false;
    bool transitioning = false;

    public AudioClip[] openCloseClip;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;

    }

    void Update()
    {
        if (lockDoor)
        {
            currentTransitionDoor.GetComponent<doorScript>().SetDoorLocked(true);
            transition = true;
            lockDoor = false;
        } else if (transition)
        {
            currentLevel.SetActive(false);
            currentLevelMaster.SetActive(false);
            pastLevelTransition.SetActive(false);
            nextLevel.SetActive(true);
            nextLevelMaster.SetActive(true);
            nextLevelTransition.SetActive(true);
            forceGun.SetOverChargeShot(1);
            StartCoroutine(BuildLevel());

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !transitioning)
        {
            lockDoor = true;
            transitioning = true;
        }
    }

    bool playNoise = true;
    IEnumerator BuildLevel()
    {
        if (playNoise)
        {
            audioSource.clip = openCloseClip[0];
            audioSource.Play();
            playNoise = false;
        }
        yield return new WaitForSeconds(5);
        audioSource.Pause();
        playNoise = true;
        currentTransitionDoor.GetComponent<doorScript>().SetDoorLocked(false);
        gameObject.SetActive(false);
    }
}
