﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class forceGun : MonoBehaviour {

    GameObject player;
    Rigidbody playerRigidbody;
    Transform playerCameraTransform;

    float shotForce = 540;
    float blastRadius = 10;
    float upwardForceAid = 0;
    static float shotCooldownSeconds = 1.25f;
    static float cooldownCounter;
    public static bool canShoot = true;
    bool shoot = false;
    bool fireModeIsFree = true;


    Transform rotatorGear;
    Transform muzzleInner;

    Renderer freeFireLight;
    Renderer bracedFireLight;
    Material freeFireLightOn;
    Material freeFireLightOff;
    Material bracedFireLightOn;
    Material bracedFireLightOff;

    Renderer multiplerIndicator1;
    Renderer multiplerIndicator2;
    Renderer multiplerIndicator3;
    Renderer multiplerIndicator4;
    Renderer multiplerIndicator5;
    Renderer multiplerIndicator6;

    public ParticleSystem shotEffect;

    Transform forceRangeGuide;
    Transform forceRange;

    public AudioClip[] soundClips;
    AudioSource soundSource;

    private void Start () {
        player = GameObject.Find("player");
        playerRigidbody = player.GetComponent<Rigidbody>();
        playerCameraTransform = GameObject.Find("playerCamera").transform;

        rotatorGear = GameObject.Find("gunBulb").transform;
        muzzleInner = GameObject.Find("muzzleInner").transform;

        freeFireLight = GameObject.Find("freeLight").GetComponent<Renderer>();
        bracedFireLight = GameObject.Find("bracedLight").GetComponent<Renderer>();
        freeFireLightOn = Resources.Load("gadjetsMaterial/freeFireOn") as Material;
        freeFireLightOff = Resources.Load("gadjetsMaterial/freeFireOff") as Material;
        bracedFireLightOn = Resources.Load("gadjetsMaterial/braceFireOn") as Material;
        bracedFireLightOff = Resources.Load("gadjetsMaterial/braceFireOff") as Material;

        multiplerIndicator1 = transform.Find("buttStock/buttStockWindows/window6").GetComponent<Renderer>();
        multiplerIndicator2 = transform.Find("buttStock/buttStockWindows/window1").GetComponent<Renderer>();
        multiplerIndicator3 = transform.Find("buttStock/buttStockWindows/window2").GetComponent<Renderer>();
        multiplerIndicator4 = transform.Find("buttStock/buttStockWindows/window3").GetComponent<Renderer>();
        multiplerIndicator5 = transform.Find("buttStock/buttStockWindows/window4").GetComponent<Renderer>();
        multiplerIndicator6 = transform.Find("buttStock/buttStockWindows/window5").GetComponent<Renderer>();

        forceRangeGuide = GameObject.Find("forceAreaCollderGuide").transform;
        forceRange = GameObject.Find("forceAreaCollder").transform;


        soundSource = GetComponent<AudioSource>();
        soundSource.loop = false;
    }

    private void Update()
    {
        //For force interaction area
        forceRange.position = forceRangeGuide.position;
        forceRange.rotation = forceRangeGuide.rotation;

        //Toggle fire type (and indicator)
        if (Input.GetMouseButtonDown(1) && !menuMaster.GetMenuOpen())
        {
            if (fireModeIsFree)
            {
                freeFireLight.material = freeFireLightOff;
                bracedFireLight.material = bracedFireLightOn;
                fireModeIsFree = false;
                soundSource.clip = soundClips[1];
                soundSource.Play();
            } else {
                freeFireLight.material = freeFireLightOn;
                bracedFireLight.material = bracedFireLightOff;
                fireModeIsFree = true;
                soundSource.clip = soundClips[1];
                soundSource.Play();
            }
        }

        if (Input.GetMouseButtonDown(0) && canShoot && !menuMaster.GetMenuOpen())
        {
            shoot = true;
        }

        ChangeMultiplierIndicator();
        
    }

    Vector3 explosionPosition;
    float forceOnPlayer = 10.5f;
    float objectForceMultipler = 3;
    float forceDistanceFromObject = 0;
    float objectRelativeForceForDistance = 0;

    float rotatorGearSpeed = 0.4f;
    float muzzleInnerSpeed = -1;

    static readonly float OVER_CHARGE_MULTIPLIER_DEFAULT = 1;
    static float overChargeMultipler = OVER_CHARGE_MULTIPLIER_DEFAULT;

    public static void SetOverChargeShot(float mutiplier)
    {
        overChargeMultipler = mutiplier;
        cooldownCounter = (shotCooldownSeconds * 60) - (0.1f * 60);
        
    }

    private void FixedUpdate()
    {


        if (shoot && !menuMaster.GetMenuOpen())
        {
            shotEffect.Play();

            //Force on player
            if (fireModeIsFree) {
                playerRigidbody.AddForce((-playerCameraTransform.forward * forceOnPlayer) * overChargeMultipler, ForceMode.Impulse);
            }

            explosionPosition = playerCameraTransform.position + playerCameraTransform.forward * forceDistanceFromObject;
            Collider[] potentialObjectsToMove = Physics.OverlapSphere(explosionPosition, blastRadius);
            foreach (Collider hit in potentialObjectsToMove)
            {
                Rigidbody objectRigidbody = hit.GetComponent<Rigidbody>();
                Transform objectTransform = hit.GetComponent<Transform>();

                Vector3 heading = hit.transform.position - playerCameraTransform.position;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                bool obscured = false;
                RaycastHit[] objectsBetween;
                Ray obscuredCheckRay = new Ray(playerCameraTransform.position, direction * distance);

                objectsBetween = Physics.RaycastAll(obscuredCheckRay, distance);

                //Checks if object is behind a force blocking object
                for (int i = 0; i < objectsBetween.Length; i++)
                {
                    if (objectsBetween[i].transform.tag != "forceInhibitIgnorer" && objectsBetween[i].transform.tag != "rangeDetector" 
                        && objectsBetween[i].transform.tag != "interactablePhysicsObject" && objectsBetween[i].transform.tag != "frictionFix" 
                        && objectsBetween[i].transform.tag != "heldObject")
                    {
                        obscured = true;
                    }
                }
                //Force on objects //Held object force handled in 'itemInteractor'
                if (objectRigidbody != null && objectRigidbody != playerRigidbody && !objectTransform.GetComponent<objectCarryInformation>().isObjHeld() 
                    && objectTransform.GetComponent<objectCarryInformation>().GetInRange() && !obscured)
                {
                    objectRigidbody.AddExplosionForce((shotForce * objectForceMultipler) * overChargeMultipler, 
                        playerCameraTransform.position + playerCameraTransform.forward * objectRelativeForceForDistance, blastRadius, upwardForceAid);
             
                }
            }

            overChargeMultipler = OVER_CHARGE_MULTIPLIER_DEFAULT;

            soundSource.clip = soundClips[0];
            soundSource.Play();

            canShoot = false;
            shoot = false;
        }
        
        //Cooldown
        if (!canShoot & cooldownCounter < shotCooldownSeconds * 60f)
        {
            cooldownCounter++;
        }
        else
        {
            canShoot = true;
            cooldownCounter = 0;
        }

        if(cooldownCounter < (shotCooldownSeconds / 3) * 60f && cooldownCounter != 0)
        {
            rotatorGearSpeed += 4f;
        } else if(rotatorGearSpeed > 0.4 && cooldownCounter != 0)
        {
            rotatorGearSpeed -= 2f;
        } else
        {
            rotatorGearSpeed = 0.4f;
        }

        rotatorGear.Rotate(0, 0, rotatorGearSpeed);
        muzzleInner.Rotate(0, 0, muzzleInnerSpeed);

    }


    public float GetShotForce()
    {
        return shotForce;
    }

    float shotBuffer = 15;
    public float GetShotBuffer()
    {
        return shotBuffer;
    }

    public void ChangeMultiplierIndicator()
    {
        if (overChargeMultipler <= 1)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOff;
            multiplerIndicator3.material = bracedFireLightOff;
            multiplerIndicator4.material = bracedFireLightOff;
            multiplerIndicator5.material = bracedFireLightOff;
            multiplerIndicator6.material = bracedFireLightOff;
        }
        else if (overChargeMultipler <= 1.4f)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOn;
            multiplerIndicator3.material = bracedFireLightOff;
            multiplerIndicator4.material = bracedFireLightOff;
            multiplerIndicator5.material = bracedFireLightOff;
            multiplerIndicator6.material = bracedFireLightOff;
        }
        else if (overChargeMultipler <= 1.8f)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOn;
            multiplerIndicator3.material = bracedFireLightOn;
            multiplerIndicator4.material = bracedFireLightOff;
            multiplerIndicator5.material = bracedFireLightOff;
            multiplerIndicator6.material = bracedFireLightOff;
        }
        else if (overChargeMultipler <= 2.2f)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOn;
            multiplerIndicator3.material = bracedFireLightOn;
            multiplerIndicator4.material = bracedFireLightOn;
            multiplerIndicator5.material = bracedFireLightOff;
            multiplerIndicator6.material = bracedFireLightOff;
        }
        else if (overChargeMultipler <= 2.6f)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOn;
            multiplerIndicator3.material = bracedFireLightOn;
            multiplerIndicator4.material = bracedFireLightOn;
            multiplerIndicator5.material = bracedFireLightOn;
            multiplerIndicator6.material = bracedFireLightOff;
        }
        else if (overChargeMultipler <= 3f)
        {
            multiplerIndicator1.material = bracedFireLightOn;
            multiplerIndicator2.material = bracedFireLightOn;
            multiplerIndicator3.material = bracedFireLightOn;
            multiplerIndicator4.material = bracedFireLightOn;
            multiplerIndicator5.material = bracedFireLightOn;
            multiplerIndicator6.material = bracedFireLightOn;
        }
    }
}
