﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class musicController : MonoBehaviour
{
    //Random order playlist
    public AudioClip[] musicPlaylist;
    public AudioSource musicSource;
    public static float audioLevel = 1;

    private static musicController objectInstance;
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (objectInstance == null)
        {
            objectInstance = this;
        }
        else
        {
            DestroyObject(gameObject);
        }
    }

    void Start()
    {
        musicSource = this.GetComponent<AudioSource>();
        musicSource.loop = false;
    }

    void Update()
    {
        //Soundtrack Volume
        musicSource.volume = audioLevel / 8;
        if (!musicSource.isPlaying)
        {
            musicSource.clip = musicPlaylist[Random.Range(0, musicPlaylist.Length)];
            musicSource.Play();
        }
    }
}