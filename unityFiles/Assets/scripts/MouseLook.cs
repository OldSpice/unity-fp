﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    float xAxisClamp = 0;

    Transform playerTransform;
    
    void Start()
    {
        playerTransform = GameObject.Find("player").transform;

    }

    void Update()
    {
        if (!menuMaster.GetMenuOpen())
        {
            RotateCamera();
        }

    }

    void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotateAmountX = mouseX * mouseAndKeyboardSettings.mouseSensitivityX;
        float rotateAmountY = mouseY * mouseAndKeyboardSettings.mouseSensitivityY;

        Vector3 targetRotateCam = transform.rotation.eulerAngles;
        Vector3 targetRotateBody = playerTransform.rotation.eulerAngles;

        targetRotateCam.x -= rotateAmountY;
        targetRotateCam.z = 0;
        targetRotateBody.y += rotateAmountX;

        xAxisClamp -= rotateAmountY;

        if(xAxisClamp > 80) {
            xAxisClamp = 80;
            targetRotateCam.x = 80;

        } else if(xAxisClamp < -80) {
            xAxisClamp = -80;
            targetRotateCam.x = -80;

        }

        transform.rotation = Quaternion.Euler(targetRotateCam);
        playerTransform.rotation = Quaternion.Euler(targetRotateBody);

    }

}




