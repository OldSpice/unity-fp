﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorScript : MonoBehaviour
{

    Transform player;

    public bool doorLocked = true;
    Renderer doorLight;
    Material doorLockedLight;
    Material doorUnlockedLight;

    Transform segmentTop;
    Transform segmentTopLeft;
    Transform segmentBottomLeft;
    Transform segmentBottom;
    Transform segmentBottomRight;
    Transform segmentTopRight;

    public AudioClip[] openCloseClip;
    private AudioSource audioSource;

    void Start()
    {
        player = GameObject.Find("player").transform;

        doorLight = transform.GetChild(0).GetChild(5).GetChild(2).gameObject.GetComponent<Renderer>();
        doorLockedLight = Resources.Load("structureMaterials/doorLocked") as Material;
        doorUnlockedLight = Resources.Load("structureMaterials/doorUnlocked") as Material;

        //Door Segments
        segmentTop = transform.GetChild(0).GetChild(3);
        segmentTopLeft = transform.GetChild(0).GetChild(4);
        segmentBottomLeft = transform.GetChild(0).GetChild(1);
        segmentBottom = transform.GetChild(0).GetChild(0);
        segmentBottomRight = transform.GetChild(0).GetChild(2);
        segmentTopRight = transform.GetChild(0).GetChild(5);
        
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;

    }

    public void SetDoorLocked(bool locked)
    {
        doorLocked = locked;
    }

    float playerDistance;
    bool closed = true;

    private void FixedUpdate()
    {
        if (doorLocked)
        {
            doorLight.material = doorLockedLight;
        }
        else
        {
            doorLight.material = doorUnlockedLight;
        }

        playerDistance = Vector3.Distance(player.position, transform.position);

        if (playerDistance < 4 && !doorLocked && closed)
        {
            audioSource.clip = openCloseClip[0];
            audioSource.Play();
            closed = false;
        }
        else if (playerDistance > 4 && !closed)
        {
            audioSource.clip = openCloseClip[0];
            audioSource.Play();
            closed = true;
        }

        if (playerDistance < 4 && !doorLocked)
        {
            //Open segments
            if (segmentTop.localPosition.y < 4.7f)
            {
                segmentTop.localPosition += new Vector3(0, 0.15f, 0);
            }
            if (segmentTopRight.localPosition.y < 3.85)
            {
                segmentTopRight.localPosition += new Vector3(0.15f, 0.075f, 0);
            }
            if (segmentTopLeft.localPosition.y < 3.7f)
            {
                segmentTopLeft.localPosition += new Vector3(-0.15f, 0.075f, 0);
            }
            if (segmentBottomRight.localPosition.y > 1.75f)
            {
                segmentBottomRight.localPosition += new Vector3(0.15f, -0.075f, 0);
            }
            if (segmentBottom.localPosition.y > 0.5f)
            {
                segmentBottom.localPosition += new Vector3(0, -0.175f, 0);
            }
            if (segmentBottomLeft.localPosition.y > 1.75f)
            {
                segmentBottomLeft.localPosition += new Vector3(-0.15f, -0.075f, 0);
            }
        }
        else
        {
            //Close segments
            if (segmentTop.localPosition.y > 3.685224f)
            {
                segmentTop.localPosition += new Vector3(0, -0.15f, 0);
            }
            if (segmentTopRight.localPosition.y > 3.439776f)
            {
                segmentTopRight.localPosition += new Vector3(-0.15f, -0.075f, 0);
            }
            if (segmentTopLeft.localPosition.y > 3.2125f)
            {
                segmentTopLeft.localPosition += new Vector3(0.15f, -0.075f, 0);
            }
            if (segmentBottomRight.localPosition.y < 2.267051f)
            {
                segmentBottomRight.localPosition += new Vector3(-0.15f, 0.075f, 0);
            }
            if (segmentBottom.localPosition.y < 1.794327f)
            {
                segmentBottom.localPosition += new Vector3(0, 0.175f, 0);
            }
            if (segmentBottomLeft.localPosition.y < 2.267051f)
            {
                segmentBottomLeft.localPosition += new Vector3(0.15f, 0.075f, 0);
            }
        }
    }
}
