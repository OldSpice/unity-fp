﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectCarryInformation : MonoBehaviour {

    public Transform respawnCannon;
    public float cannonPower = 15;

    public float weight;
    public bool canTake;
    public bool canHold;
    public string itemName;
    bool isHeld;
    bool isInRange = false;

    public float GetWeight() {
        return weight;
    }

    public string ItemName()
    {
        return itemName;
    }

    public void SetIsHeld(bool isHeld)
    {
        this.isHeld = isHeld;
    }
    public bool isObjHeld()
    {
        return isHeld;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "rangeDetector")
        {
            isInRange = true;
        }
        if (other.gameObject.tag == "killZone" || other.gameObject.tag == "cubeKiller")
        {
            ResetItemIntoLevel();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "rangeDetector")
        {
            isInRange = false;

        }
    }
    public bool GetInRange()
    {
        return isInRange;
    }
    private void FixedUpdate()
    {
        if (transform.position.y < -60)
        {
            ResetItemIntoLevel();
        }
    }
    private void ResetItemIntoLevel()
    {
        try
        {
            transform.position = respawnCannon.position;
            transform.GetComponent<Rigidbody>().velocity = respawnCannon.forward * cannonPower;
        } catch (UnassignedReferenceException)
        {
            transform.gameObject.SetActive(false);
        }
    }
}
